.PHONY: all clean build

all: clean build

clean:
	rm -rf node_modules

build:
	npm install
	npm run build
