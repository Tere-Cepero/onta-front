# mapita

La interfaz del proyecto **Onta**. Diseñara para su uso en web, dando gran
importancia a que funcione en celulares. Posiblemente crear una web app
progresiva sea una buena idea.

## Historias de usuario

* Encontrarme en el mapa bajo demanda (no pedir ubicación por defecto)
* Escribir una dirección y ver posibles rutas
* Ver rutas cercanas
* Ver paradas cercanas
* Buscar rutas por nombre
* Buscar rutas por lugares por los que pasa
* poder compartir url de las rutas que pasan por un lugar en especial

## Desarrollo

``` bash
# Instalar dependencias
npm install

# iniciar servidor de desarrollo
npm run dev
```
